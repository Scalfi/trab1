import React from 'react';
import { Platform,StyleSheet, Text, View,  } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Constants } from 'expo';
import { Ionicons } from '@expo/vector-icons';

import Calculadora from './src/Calculadora/Calculadora'
import Feed from './src/Feed/Feed'
import Instagram from './src/Instagram/Instagram'
import Maps from './src/Maps/Maps'
import GitApi from './src/GitHub/GitApi'

class Home extends React.Component {
  static navigationOptions = {
    title: 'Home',
    drawerIcon: ({ focused }) => (
      <Ionicons name="md-home" size={24} color={focused ? 'blue' : 'black'} />
    ),
  }; 

  render() {
    return (
      <View style={styles.container}>

        <Text
          style={styles.paragraph}
          onPress={() => {
            this.props.navigation.toggleDrawer();
          }}>
          Toggle Drawer
        </Text>
      </View>
    );
  }
}


const navigator = createDrawerNavigator(
  {
    Home,
    Instagram,
    Calculadora,
    Feed,
    Maps,
    GitApi,
  },
  {
  }
);

export default createAppContainer(navigator);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  image: {
    flex: 1,
    height: 300,
  },
});