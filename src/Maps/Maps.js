import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    TextInput,
    Dimensions,
    Image,
    StyleSheet,
    Card, CardItem
} from 'react-native';

import MapView from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import getDirections from 'react-native-google-maps-directions'
import { PermissionsAndroid } from 'react-native';

import Geocoder from 'react-native-geocoding';

const GOOGLE_MAPS_APIKEY = 'AIzaSyDUrCl21U2Z6DDwu0a1QL4dS3EgQnbbN38';

const backgroundColor = '#007256';

const { height, width } = Dimensions.get('window');

export default class Maps extends Component {

    state = {

        origin: { latitude: 42.3616132, longitude: -71.0672576 },
        destination: { latitude: 42.3730591, longitude: -71.033754 },
        originText: '',
        destinationText: '',
        distanceText: '',
        destinationText2: '',

    };


    handleButton = () => {
        Geocoder.init(GOOGLE_MAPS_APIKEY);


        if (this.state.originText != '') {
            console.log('origin')

            Geocoder.from(this.state.originText)
                .then(json => {
                    var location = json.results[0].geometry.location;
                    console.log(location);
                    this.setState({ origin: { latitude: location.lat, longitude: location.lng } });

                })
                .catch(error => console.warn(error));

        }

        else {

            alert("Digite a origem ! ")

        }

        if (this.state.destinationText != '') {
            console.log('destino')


            Geocoder.from(this.state.destinationText)
                .then(json => {
                    var location = json.results[0].geometry.location;
                    console.log(location);
                    this.setState({ destination: { latitude: location.lat, longitude: location.lng } });

                })
                .catch(error => console.warn(error));
        }

        else {

            alert("Digite o destino ! ")

        }

    }

    handleGetGoogleMapDirections = () => {

        const data = {

            source: this.state.origin,
            destination: this.state.destination,
            params: [
                {
                    key: "travelmode",
                    value: "driving"
                }
            ]

        };

        getDirections(data)

    };

    onStart = (result) => {
        console.log(result);

    }
    onReady = (result) => {
        console.log('Distance: km ' + result.distance)
        console.log('Duration: ${} min.  ' + result.duration)

        this.setState({
            distanceText: 'Distance: km ' + result.distance,
            destinationText2: 'Duration: min. ' + parseFloat(result.duration).toFixed(2),
        })


    }
    render() {

        return (

            <View style={styles.container}>


                <MapView

                    ref={map => this.mapView = map}
                    style={styles.map}

                    region={{
                        latitude: (this.state.origin.latitude + this.state.destination.latitude) / 2,
                        longitude: (this.state.origin.longitude + this.state.destination.longitude) / 2,
                        latitudeDelta: Math.abs(this.state.origin.latitude - this.state.destination.latitude) + Math.abs(this.state.origin.latitude - this.state.destination.latitude) * .1,
                        longitudeDelta: Math.abs(this.state.origin.longitude - this.state.destination.longitude) + Math.abs(this.state.origin.longitude - this.state.destination.longitude) * .1,
                    }}

                    loadingEnabled={true}
                    toolbarEnabled={true}
                    zoomControlEnabled={true}

                >

                    <MapView.Marker
                        coordinate={this.state.destination}
                    >
                        <MapView.Callout onPress={this.handleGetGoogleMapDirections}>
                            <Text>Destino</Text>

                        </MapView.Callout>
                    </MapView.Marker>

                    <MapView.Marker
                        coordinate={this.state.origin}
                    >

                        <MapView.Callout>
                            <Text>VOcê esta aqui</Text>
                        </MapView.Callout>
                    </MapView.Marker>

                    <MapViewDirections
                        origin={this.state.origin}
                        destination={this.state.destination}
                        apikey={GOOGLE_MAPS_APIKEY}
                        onStart={this.onReady}
                        onReady={this.onReady}
                        strokeWidth={3}
                        strokeColor="blue"
                    />

                </MapView>

                <View style={styles.inputContainer}>
                    <Text style={{padding : 5}}>{this.state.distanceText}</Text>
                    <Text style={{padding : 5}}>{this.state.destinationText2}</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ originText: text })}
                        placeholder='Origem'
                        value={this.state.originText}
                    />

                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ destinationText: text })}
                        placeholder='Destino'
                        value={this.state.destinationText}
                    />

                    <TouchableOpacity style={styles.button} onPress={this.handleButton}>

                        <Text style={styles.buttonText}>Buscar rota</Text>

                    </TouchableOpacity>

                </View>

            </View>

        );

    }

}

const styles = StyleSheet.create({

    container: {

        flex: 3,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',

    },

    map: {
        flex: 3,
        height,
        width
    },

    button: {

        width: width - 100,
        height: 40,
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#000',
        borderRadius: 7,
        marginBottom: 15,
        marginHorizontal: 20,

    },

    buttonText: {

        color: '#000',
        fontWeight: 'bold',

    },

    inputContainer: {

        width: '100%',
        maxHeight: 200,

    },

    input: {

        width: width - 40,
        maxHeight: 200,
        backgroundColor: '#FFF',
        marginBottom: 15,
        marginHorizontal: 20,

    },

});