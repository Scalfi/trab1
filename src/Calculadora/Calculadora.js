import * as React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';


export default class Calculadora extends React.Component {
	
	state = {
		valor1: "",
	}
	
	apertei = (valor) => {
    const number = Number(this.state.valor1)
    if (Number.isInteger(number)) {
      this.setState({
        valor1: this.state.valor1 + valor ,
      })
    } else {
       this.setState({
        valor1: valor ,
      })
    }
	} 
	
  limpar = () => {
		this.setState({
			valor1: "",
		})
	}

  igual = () => {
    this.setState({
			valor1: eval(this.state.valor1),
		})

  }

  maismenos = () => {
    const valor = parseFloat(this.state.valor1)*-1
    this.setState({
			valor1: valor,
		})

  }

  porcentagem = () => {
    const valor = parseFloat(this.state.valor1)/100
    this.setState({
			valor1: valor,
		})

  }


  render() {
    return (
      <View style={styles.container}>

        <View style={styles.container2}>
            <Text style={{color: 'white'}}>  </Text>
        </View>

        <View style={styles.container2_1}>
            <Text style={{color: 'white', fontSize: 24}}> 
				{this.state.valor1}
			</Text>
        </View>

            <View style={styles.container3}> 

              <View style={{flexDirection: 'row', flex: 1}}>
                <TouchableOpacity style={styles.botaoCinzac} onPress={() => this.limpar()}> 
                  <Text style={{fontSize: 30}}> AC </Text>                               
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinzac} onPress={() => this.maismenos()}>
                  <Text style={{fontSize: 30}}> +/- </Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinzac} onPress={() => this.porcentagem()}>
                  <Text style={{fontSize: 30}}> % </Text> 
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoLaranja}onPress={() => this.apertei("/")}>
                  <Text style={{fontSize: 30}}> / </Text> 
                </TouchableOpacity>
              </View>


              <View style={{flexDirection: 'row', flex: 1}}>
                <TouchableOpacity style={styles.botaoCinza} onPress={() => this.apertei("7")}>
					      <Text style={{fontSize: 30}}>	7 </Text>                               
                </TouchableOpacity>

              
                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("8")}>
                <Text style={{fontSize: 30}}> 8 </Text> 
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("9")}>
                  <Text style={{fontSize: 30}}> 9 </Text>  
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoLaranja} onPress={() => this.apertei("-")}>
                  <Text style={{fontSize: 30}}> - </Text>  
                </TouchableOpacity>  
              </View>
             


              <View style={{flexDirection: 'row', flex: 1}}>
                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("4")}>
                  <Text style={{fontSize: 30}}> 4 </Text>                              
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("5")}>
                  <Text style={{fontSize: 30}}> 5 </Text>  
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("6")}>
                  <Text style={{fontSize: 30}}> 6 </Text>  
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoLaranja} onPress={() => this.apertei("+")}>
                  <Text style={{fontSize: 30}}> + </Text> 
                </TouchableOpacity>
              </View>


              <View style={{flexDirection: 'row', flex: 1}}>
                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("1")}>
                  <Text style={{fontSize: 30}}> 1 </Text>                               
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("2")}>
                  <Text style={{fontSize: 30}}> 2 </Text>  
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei("3")}>
                  <Text style={{fontSize: 30}}> 3 </Text>  
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoLaranja} onPress={() => this.apertei("*")}>
                  <Text style={{fontSize: 30}}> x </Text>  
                </TouchableOpacity>
              </View>


              <View style={{flexDirection: 'row', flex: 1}}>

                <TouchableOpacity style={styles.botaoGrandao}onPress={() => this.apertei("0")}>
                  <Text style={{fontSize: 30}}> 0 </Text>                               
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoCinza}onPress={() => this.apertei(".")}>
                  <Text style={{fontSize: 30}}> . </Text>                  
                </TouchableOpacity>

                <TouchableOpacity style={styles.botaoLaranja} onPress={() => this.igual()} >
                  <Text style={{fontSize: 30}}> = </Text>                 
                </TouchableOpacity>
               
              </View>

          </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'black',
    padding: 8,
  },

  container2: {
    flex: 1,
    textAlign: 'right',
    backgroundColor: 'black',
    textDecorationColor: 'white',
    padding: 2,
  },

  container2_1: {
    flex: 1,
    textAlign: 'right',
    backgroundColor: 'black',
    padding: 2,
  },

  container3: {
    flex: 5,
    backgroundColor: 'black',
    padding: 2,
  },
  
  botaoCinzac: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
    backgroundColor: 'silver',
    margin: 1,
    padding: 2,
    color: "red",
  },

  botaoCinza: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    
    backgroundColor: '#A4A4A4',
    margin: 1,
    padding: 2,
   
  },
  
  botaoLaranja: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF8000',
    margin: 1,
    padding: 2,
  },


  botaoGrandao: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#A4A4A4',
    margin: 1,
    padding: 5,
  },



  
});