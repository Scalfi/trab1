import React from 'react'
import { ScrollView, View, Text, Image } from 'react-native'
import axios from 'axios';
import { Card, CardItem, Button, Icon, 
  Left, Body, Right } from 'native-base'

const base = require('./dados.json')

export default class Feed extends React.Component {
  state = {
    dados: []
  }

componentDidMount(){
  this.receberDados()
}

receberDados = async () => {
  let retorno = await axios.get('https://jsonplaceholder.typicode.com/photos')
  this.setState({
    dados: retorno.data
  }) 
}

  render(){
    return(
    <ScrollView> 
      { this.state.dados.slice(0,25).map(( linha,key ) => {
          return(
            <Card key={key}>
              <CardItem>
                 <Text> {linha.title} </Text>
              </CardItem>
             <CardItem>
               <Image 
                  source={{ uri: linha.thumbnailUrl }}
                  style = {{ height: 200, width: null, flex: 1 }}
                />
              </CardItem>
            </Card>          
          )
      } ) }


    </ScrollView>
    )
  }
}