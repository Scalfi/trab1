import React, { Component } from 'react';
import { Text, View, StyleSheet, TextInput, TouchableOpacity, FlatList, SafeAreaView } from 'react-native';
import axios from 'axios';

import { Card } from 'react-native-paper';

export default class GitApi extends React.Component {
    state = {
        search: "",
        list: [{
            id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
            name: 'First Item',
        }],
    }

    receberDados = async () => {
        let repositories = [];
        let response = await fetch('https://api.github.com/users/'+ this.state.search.toLowerCase() +'/repos');

  
        if (true === response.ok) {
          repositories = await response.json();
          this.setState({list:repositories})
        } else {
          error = true;
          message = 'Usuário não encontrado';
        }

    }


    render() {
        const DATA = [
            {
                id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
                title: 'First Item',
            },
            {
                id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
                title: 'Second Item',
            },
            {
                id: '58694a0f-3da1-471f-bd96-145571e29d72',
                title: 'Third Item',
            },
        ];

        function Item({ title }) {
            return (
                <View style={styles.item}>
                    <Text style={styles.title}>{title}</Text>
                </View>
            );
        }
        console.log(this.state.list)

        return (
            <View style={styles.container}>
                <View style={styles.inputContainer}>


                    <TextInput
                        style={{ backgroundColor: "#fff", padding: 10, margin: 2 }}
                        onChangeText={(text) => this.setState({ search: text })}
                        placeholder='Pesquise aqui'
                        value={this.state.search}
                    />
                    <TouchableOpacity style={styles.button} onPress={this.receberDados}>
                        <Text style={styles.buttonText}>Buscar rota</Text>
                    </TouchableOpacity>
                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={this.state.list}
                        renderItem={({ item }) => <Item title={item.name} />}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
    buttonText: {
        color: '#000',
        fontWeight: 'bold',
    },
    inputContainer: {
        width: '100%',
        maxHeight: 200,
        flex: 2
    },
    button: {
        backgroundColor: '#FFF',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: '#000',
        borderRadius: 7,
        marginBottom: 15,
        marginHorizontal: 20,
        margin: 5

    },



});